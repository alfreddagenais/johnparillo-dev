


<!DOCTYPE html>
<!-- xlsx.js (C) 2013-2015 SheetJS http://sheetjs.com -->
<!-- vim: set ts=2: -->


<html xmlns:mso="urn:schemas-microsoft-com:office:office" xmlns:msdt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

<title>Nouveau Billet SEM</title>
<style>
#drop{
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	padding:0;
	text-align:center;
	font:20pt bold,"Vollkorn";color:#bbb;
}


html, body {
	margin: 0;
	padding: 0;
	background-attachment: fixed;
	background-position: 50% 50%;
	background-size: cover;
}
a {
	text-decoration: underline;
}
a:hover {
	text-decoration: none;
}


.body {
	max-width: 600px;
	margin: 0 auto;
	padding: 40px;
}
.body-s {
	max-width: 600px;
}
@media screen and (max-width: 600px) {
	.body {
		padding: 20px;
	}
}


@import url(http://fonts.googleapis.com/css?family=Open+Sans:300,400,700);

/**/
/* defaults */
/**/
.sky-form {
	margin: 0;
	outline: none;
	box-shadow: 0 0 20px rgba(0,0,0,.3);
	font: 13px/1.55 'Open Sans', Helvetica, Arial, sans-serif;
	color: #666;
}
.sky-form * {
	margin: 0;
	padding: 0;
}
.sky-form header {
	display: block;
	padding: 20px 30px;	
	border-bottom: 1px solid rgba(0,0,0,.1);
	background: rgba(248,248,248,.9);
	font-size: 25px;
	font-weight: 300;
	color: #232323;
}
.sky-form fieldset {
	display: block;	
	padding: 25px 30px 5px;
	border: none;
	background: rgba(255,255,255,.9);
}
.sky-form fieldset + fieldset {
	border-top: 1px solid rgba(0,0,0,.1);
}
.sky-form section {
	margin-bottom: 20px;
}
.sky-form footer {
	display: block;
	padding: 15px 30px 25px;
	border-top: 1px solid rgba(0,0,0,.1);
	background: rgba(248,248,248,.9);
}
.sky-form footer:after {
	content: '';
	display: table;
	clear: both;
}
.sky-form a {
	color: #2da5da;
}
.sky-form .label {
	display: block;
	margin-bottom: 6px;
	line-height: 19px;
}
.sky-form .label.col {
	margin: 0;
	padding-top: 10px;
}
.sky-form .note {
	margin-top: 6px;
	padding: 0 1px;
	font-size: 11px;
	line-height: 15px;
	color: #999;
}
.sky-form .input,
.sky-form .select,
.sky-form .textarea,
.sky-form .radio,
.sky-form .checkbox,
.sky-form .toggle,
.sky-form .button {
	position: relative;
	display: block;
}
.sky-form .input input,
.sky-form .select select,
.sky-form .textarea textarea {
	display: block;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	width: 100%;
	height: 39px;
	padding: 8px 10px;
	outline: none;
	border-width: 2px;
	border-style: solid;
	border-radius: 0;
	background: #fff;
	font: 15px/19px 'Open Sans', Helvetica, Arial, sans-serif;
	color: #404040;
	appearance: normal;
	-moz-appearance: none;
	-webkit-appearance: none;
}




/**/
/* file inputs */
/**/
.sky-form .input-file .button {
	position: absolute;
	top: 4px;
	right: 4px;
	float: none;
	height: 31px;
	margin: 0;
	padding: 0 20px;
	font-size: 13px;
	line-height: 31px;
}
.sky-form .input-file .button:hover {
	box-shadow: none;
}
.sky-form .input-file .button input {
	position: absolute;
	top: 0;
	right: 0;
	padding: 0;
	font-size: 30px;
	cursor: pointer;
	opacity: 0;
}


/**/
/* selects */
/**/
.sky-form .select i {
	position: absolute;
	top: 14px;
	right: 14px;
	width: 5px;
	height: 11px;
	background: #fff;
	box-shadow: 0 0 0 12px #fff;
}
.sky-form .select i:after,
.sky-form .select i:before {
	content: '';
	position: absolute;
	right: 0;
	border-right: 4px solid transparent;
	border-left: 4px solid transparent;
}
.sky-form .select i:after {
	bottom: 0;
	border-top: 4px solid #404040;
}
.sky-form .select i:before {
	top: 0;
	border-bottom: 4px solid #404040;
}
.sky-form .select-multiple select {
	height: auto;
}


/**/
/* textareas */
/**/


.sky-form .textarea textarea {
	height: 475px;
	resize: vertical;	
}

/*.sky-form .textarea textarea {
	height: auto;
	resize: none;
}*/
.sky-form .textarea-resizable textarea {
	resize: vertical;	
}
.sky-form .textarea-expandable textarea {
	height: 39px;
}
.sky-form .textarea-expandable textarea:focus {
	height: auto;
}


/**/
/* radios and checkboxes */
/**/
.sky-form .radio,
.sky-form .checkbox {
	margin-bottom: 4px;
	padding-left: 27px;
	font-size: 15px;
	line-height: 27px;
	color: #404040;
	cursor: pointer;
}
.sky-form .radio:last-child,
.sky-form .checkbox:last-child {
	margin-bottom: 0;
}
.sky-form .radio input,
.sky-form .checkbox input {
	position: absolute;
	left: -9999px;
}
.sky-form .radio i,
.sky-form .checkbox i {
	position: absolute;
	top: 5px;
	left: 0;
	display: block;
	width: 13px;
	height: 13px;
	outline: none;
	border-width: 2px;
	border-style: solid;
	background: #fff;
}
.sky-form .radio i {
	border-radius: 50%;
}
.sky-form .radio input + i:after,
.sky-form .checkbox input + i:after {
	position: absolute;
	opacity: 0;
	transition: opacity 0.1s;
	-o-transition: opacity 0.1s;
	-ms-transition: opacity 0.1s;
	-moz-transition: opacity 0.1s;
	-webkit-transition: opacity 0.1s;
}
.sky-form .radio input + i:after {
	content: '';
	top: 4px;
	left: 4px;
	width: 5px;
	height: 5px;
	border-radius: 50%;
}
.sky-form .checkbox input + i:after {
	content: '\f00c';
	top: -1px;
	left: -1px;
	width: 15px;
	height: 15px;
	font: normal 12px/16px FontAwesome;
	text-align: center;
}
.sky-form .radio input:checked + i:after,
.sky-form .checkbox input:checked + i:after {
	opacity: 1;
}
.sky-form .inline-group {
	margin: 0 -30px -4px 0;
}
.sky-form .inline-group:after {
	content: '';
	display: table;
	clear: both;
}
.sky-form .inline-group .radio,
.sky-form .inline-group .checkbox {
	float: left;
	margin-right: 30px;
}
.sky-form .inline-group .radio:last-child,
.sky-form .inline-group .checkbox:last-child {
	margin-bottom: 4px;
}


/**/
/* toggles */
/**/
.sky-form .toggle {
	margin-bottom: 4px;
	padding-right: 61px;
	font-size: 15px;
	line-height: 27px;
	color: #404040;
	cursor: pointer;
}
.sky-form .toggle:last-child {
	margin-bottom: 0;
}
.sky-form .toggle input {
	position: absolute;
	left: -9999px;
}
.sky-form .toggle i {
	content: '';
	position: absolute;
	top: 4px;
	right: 0;
	display: block;
	width: 49px;
	height: 17px;
	border-width: 2px;
	border-style: solid;
	border-radius: 12px;
	background: #fff;
}
.sky-form .toggle i:after {
	content: 'OFF';
	position: absolute;
	top: 2px;
	right: 8px;
	left: 8px;
	font-style: normal;
	font-size: 9px;
	line-height: 13px;
	font-weight: 700;
	text-align: left;
	color: #5f5f5f;
}
.sky-form .toggle i:before {
	content: '';
	position: absolute;
	z-index: 1;
	top: 4px;
	right: 4px;
	display: block;
	width: 9px;
	height: 9px;
	border-radius: 50%;
	opacity: 1;
	transition: right 0.2s;
	-o-transition: right 0.2s;
	-ms-transition: right 0.2s;
	-moz-transition: right 0.2s;
	-webkit-transition: right 0.2s;
}
.sky-form .toggle input:checked + i:after {
	content: 'ON';
	text-align: right;
}
.sky-form .toggle input:checked + i:before {
	right: 36px;
}


/**/
/* ratings */
/**/
.sky-form .rating {
	margin-bottom: 4px;
	font-size: 15px;
	line-height: 27px;
	color: #404040;
}
.sky-form .rating:last-child {
	margin-bottom: 0;
}
.sky-form .rating input {
	position: absolute;
	left: -9999px;
}
.sky-form .rating label {
	display: block;
	float: right;
	height: 17px;
	margin-top: 5px;
	padding: 0 2px;
	font-size: 17px;
	line-height: 17px;
	cursor: pointer;
}


/**/
/* buttons */
/**/
.sky-form .button {
	float: right;
	height: 39px;
	overflow: hidden;
	margin: 10px 0 0 20px;
	padding: 0 25px;
	outline: none;
	border: 0;
	font: 300 15px/39px 'Open Sans', Helvetica, Arial, sans-serif;
	text-decoration: none;
	color: #fff;
	cursor: pointer;
}


/**/
/* icons */
/**/
@font-face
{
	font-family: 'FontAwesome';
	src: url('../icons/fontawesome-webfont.eot?v=3.0.1');
	src: url('../icons/fontawesome-webfont.eot?#iefix&v=3.0.1') format('embedded-opentype'),
	url('../icons/fontawesome-webfont.woff?v=3.0.1') format('woff'),
	url('../icons/fontawesome-webfont.ttf?v=3.0.1') format('truetype');
	font-weight: normal;
	font-style: normal;
}

.sky-form [class^="icon-"] {
  font-family: FontAwesome;
  font-style: normal;
  font-weight: normal;
  -webkit-font-smoothing: antialiased;
}
.sky-form .icon-append,
.sky-form .icon-prepend {
	position: absolute;
	top: 5px;
	width: 29px;
	height: 29px;
	font-size: 15px;
	line-height: 29px;
	text-align: center;
}
.sky-form .icon-append {
	right: 5px;
	padding-left: 3px;
	border-left-width: 1px;
	border-left-style: solid;
}
.sky-form .icon-prepend {
	left: 5px;
	padding-right: 3px;
	border-right-width: 1px;
	border-right-style: solid;
}
.sky-form .input .icon-prepend + input,
.sky-form .textarea .icon-prepend + textarea {
	padding-left: 46px;
}
.sky-form .input .icon-append + input,
.sky-form .textarea .icon-append + textarea {
	padding-right: 46px;
}
.sky-form .input .icon-prepend + .icon-append + input,
.sky-form .textarea .icon-prepend + .icon-append + textarea {
	padding-left: 46px;
}


/**/
/* grid */
/**/
.sky-form .row {
	margin: 0 -15px;
}
.sky-form .row:after {
	content: '';
	display: table;
	clear: both;
}
.sky-form .col {
	float: left;
	min-height: 1px;
	padding-right: 15px;
	padding-left: 15px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
}
.sky-form .col-1 {
	width: 8.33%;
}
.sky-form .col-2 {
	width: 16.66%;
}
.sky-form .col-3 {
	width: 25%;
}
.sky-form .col-4 {
	width: 33.33%;
}
.sky-form .col-5 {
	width: 41.66%;
}
.sky-form .col-6 {
	width: 50%;
}
.sky-form .col-8 {
	width: 66.67%;
}
.sky-form .col-9 {
	width: 75%;
}
.sky-form .col-10 {
	width: 83.33%;
}
@media screen and (max-width: 600px) {
	.sky-form .col {
		float: none;
		width: 100%;
	}
}



/**/
/* normal state */
/**/
.sky-form .input input,
.sky-form .select select,
.sky-form .textarea textarea,
.sky-form .radio i,
.sky-form .checkbox i,
.sky-form .toggle i,
.sky-form .icon-append,
.sky-form .icon-prepend {
	border-color: #e5e5e5;
	transition: border-color 0.3s;
	-o-transition: border-color 0.3s;
	-ms-transition: border-color 0.3s;
	-moz-transition: border-color 0.3s;
	-webkit-transition: border-color 0.3s;
}
.sky-form .toggle i:before {
	background-color: #2da5da;	
}
.sky-form .rating label {
	color: #ccc;
	transition: color 0.3s;
	-o-transition: color 0.3s;
	-ms-transition: color 0.3s;
	-moz-transition: color 0.3s;
	-webkit-transition: color 0.3s;
}
.sky-form .button {
	background-color: #2da5da;
	opacity: 0.8;
	transition: opacity 0.2s;
	-o-transition: opacity 0.2s;
	-ms-transition: opacity 0.2s;
	-moz-transition: opacity 0.2s;
	-webkit-transition: opacity 0.2s;
}
.sky-form .button.button-secondary {
	background-color: #b3b3b3;
}
.sky-form .icon-append,
.sky-form .icon-prepend {
	color: #ccc;
}


/**/
/* hover state */
/**/
.sky-form .input:hover input,
.sky-form .select:hover select,
.sky-form .textarea:hover textarea,
.sky-form .radio:hover i,
.sky-form .checkbox:hover i,
.sky-form .toggle:hover i {
	border-color: #8dc9e5;
}
.sky-form .rating input + label:hover,
.sky-form .rating input + label:hover ~ label {
	color: #2da5da;
}
.sky-form .button:hover {
	opacity: 1;
}


/**/
/* focus state */
/**/
.sky-form .input input:focus,
.sky-form .select select:focus,
.sky-form .textarea textarea:focus,
.sky-form .radio input:focus + i,
.sky-form .checkbox input:focus + i,
.sky-form .toggle input:focus + i {
	border-color: #2da5da;
}


/**/
/* checked state */
/**/
.sky-form .radio input + i:after {
	background-color: #2da5da;	
}
.sky-form .checkbox input + i:after {
	color: #2da5da;
}
.sky-form .radio input:checked + i,
.sky-form .checkbox input:checked + i,
.sky-form .toggle input:checked + i {
	border-color: #2da5da;	
}
.sky-form .rating input:checked ~ label {
	color: #2da5da;	
}


/**/
/* error state */
/**/
.sky-form .state-error input,
.sky-form .state-error select,
.sky-form .state-error textarea,
.sky-form .radio.state-error i,
.sky-form .checkbox.state-error i,
.sky-form .toggle.state-error i {
	background: #fff0f0;
}
.sky-form .state-error select + i {
	background: #fff0f0;
	box-shadow: 0 0 0 12px #fff0f0;
}
.sky-form .toggle.state-error input:checked + i {
	background: #fff0f0;
}
.sky-form .note-error {
	color: #ee9393;	
}


/**/
/* success state */
/**/
.sky-form .state-success input,
.sky-form .state-success select,
.sky-form .state-success textarea,
.sky-form .radio.state-success i,
.sky-form .checkbox.state-success i,
.sky-form .toggle.state-success i {
	background: #f0fff0;
}
.sky-form .state-success select + i {
	background: #f0fff0;
	box-shadow: 0 0 0 12px #f0fff0;
}
.sky-form .toggle.state-success input:checked + i {
	background: #f0fff0;
}
.sky-form .note-success {
	color: #6fb679;
}


/**/
/* disabled state */
/**/
.sky-form .input.state-disabled input,
.sky-form .select.state-disabled,
.sky-form .textarea.state-disabled,
.sky-form .radio.state-disabled,
.sky-form .checkbox.state-disabled,
.sky-form .toggle.state-disabled,
.sky-form .button.state-disabled {
	cursor: default;
	opacity: 0.5;
}
.sky-form .input.state-disabled:hover input,
.sky-form .select.state-disabled:hover select,
.sky-form .textarea.state-disabled:hover textarea,
.sky-form .radio.state-disabled:hover i,
.sky-form .checkbox.state-disabled:hover i,
.sky-form .toggle.state-disabled:hover i {
	border-color: #e5e5e5;
}

</style>

<!--[if gte mso 9]><![endif]-->
</head>
<body class="bg-cyan">
<select name="format" style="display:none">
<option value="csv"> CSV</option>
<option value="json" selected> JSON</option>
<option value="form"> FORMULAE</option>
</select><br />


<div id="drop">


		<div class="body body-s">
		
			<form id="sem-form" onsubmit="gotoURL(); return false;" class="sky-form">
				<header>Nouveau billet SEM</header>
				
				<fieldset>					
					
<!--

http://jira.transcontinental.ca/secure/CreateIssueDetails!init.jspa?pid=11076&issuetype=39&summary=test+discard+lorem+ipsum+test&description=description+goes+here&components=11292&customfield_10233=7%2FDec%2F15&customfield_10867=6%2FDec%2F16&customfield_10873=500
 
-->					

					<section><label class="input"><input type="text" id="i_company" placeholder="Nom du client"></label></section>
					
					
					<section><label class="input"><input type="text" id="i_pbs" placeholder="PBS #"></label></section>

					<section><label class="input"><input type="text" id="i_startDate" placeholder="Start Date"></label></section>
					<section><label class="input"><input type="text" id="i_endDate" placeholder="End Date"></label></section>

					<section><label class="input"><input type="text" id="i_spendType" placeholder="Type de dépense"></label></section>
					<section><label class="input"><input type="text" id="i_monthlyBudget" placeholder="Budget Mensuel"></label></section>
					<section><label class="input"><input type="text" id="i_varBudget" placeholder="Budget sur mesure "></label></section>
					<section><label class="input"><input type="text" id="i_managementfee" placeholder="Frais de gestion"></label></section>

					<section><label class="input"><input type="text" id="i_territory" placeholder="Territoire"></label></section>
					<section><label class="input"><input type="text" id="i_campaign_lang" placeholder="Languages"></label></section>
					
					<section><label class="textarea"><textarea type="text" id="i_description" placeholder="Description"></textarea></label></section>


					<section style="display: none;">
						<label class="select">
							<select>
								<option value="0" selected disabled>Gender</option>
								<option value="1">Male</option>
								<option value="2">Female</option>
								<option value="3">Other</option>
							</select>
							<i></i>
						</label>
					</section>

				</fieldset>
				<footer>
					<button type="submit" class="button">Submit</button>
					<input type="hidden" name="jira-url" id="jira-url" />
				</footer>
			</form>
			
		</div>

</div>

<p><input type="file" name="xlfile" id="xlf" style="display: none" /></p>

<input type="checkbox" name="useworker" checked style="display:none" />
<input type="checkbox" name="xferable" checked style="display:none" />
<input type="checkbox" name="userabs" checked style="display:none" />

<pre id="sem-output" style="display: none;"></pre>
<pre id="out" style="display: none;"></pre>
<br />
<!-- uncomment the next line here and in xlsxworker.js for encoding support -->
<!--<script src="dist/cpexcel.js"></script>-->
<script src="shim.js"></script>
<script src="jszip.js"></script>
<script src="xlsx.js"></script>
<!-- uncomment the next line here and in xlsxworker.js for ODS support -->
<script src="dist/ods.js"></script>
<script>
var X = XLSX;
var XW = {
	/* worker message */
	msg: 'xlsx',
	/* worker scripts */
	rABS: './xlsxworker2.js',
	norABS: './xlsxworker1.js',
	noxfer: './xlsxworker.js'
};

var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";
if(!rABS) {
	document.getElementsByName("userabs")[0].disabled = true;
	document.getElementsByName("userabs")[0].checked = false;
}

var use_worker = typeof Worker !== 'undefined';
if(!use_worker) {
	document.getElementsByName("useworker")[0].disabled = true;
	document.getElementsByName("useworker")[0].checked = false;
}

var transferable = use_worker;
if(!transferable) {
	document.getElementsByName("xferable")[0].disabled = true;
	document.getElementsByName("xferable")[0].checked = false;
}

var wtf_mode = false;

function fixdata(data) {
	var o = "", l = 0, w = 10240;
	for(; l<data.byteLength/w; ++l) o+=String.fromCharCode.apply(null,new Uint8Array(data.slice(l*w,l*w+w)));
	o+=String.fromCharCode.apply(null, new Uint8Array(data.slice(l*w)));
	return o;
}

function ab2str(data) {
	var o = "", l = 0, w = 10240;
	for(; l<data.byteLength/w; ++l) o+=String.fromCharCode.apply(null,new Uint16Array(data.slice(l*w,l*w+w)));
	o+=String.fromCharCode.apply(null, new Uint16Array(data.slice(l*w)));
	return o;
}

function s2ab(s) {
	var b = new ArrayBuffer(s.length*2), v = new Uint16Array(b);
	for (var i=0; i != s.length; ++i) v[i] = s.charCodeAt(i);
	return [v, b];
}

function xw_noxfer(data, cb) {
	var worker = new Worker(XW.noxfer);
	worker.onmessage = function(e) {
		switch(e.data.t) {
			case 'ready': break;
			case 'e': console.error(e.data.d); break;
			case XW.msg: cb(JSON.parse(e.data.d)); break;
		}
	};
	var arr = rABS ? data : btoa(fixdata(data));
	worker.postMessage({d:arr,b:rABS});
}

function xw_xfer(data, cb) {
	var worker = new Worker(rABS ? XW.rABS : XW.norABS);
	worker.onmessage = function(e) {
		switch(e.data.t) {
			case 'ready': break;
			case 'e': console.error(e.data.d); break;
			default: xx=ab2str(e.data).replace(/\n/g,"\\n").replace(/\r/g,"\\r"); console.log("done"); cb(JSON.parse(xx)); break;
		}
	};
	if(rABS) {
		var val = s2ab(data);
		worker.postMessage(val[1], [val[1]]);
	} else {
		worker.postMessage(data, [data]);
	}
}

function xw(data, cb) {
	transferable = document.getElementsByName("xferable")[0].checked;
	if(transferable) xw_xfer(data, cb);
	else xw_noxfer(data, cb);
}

function get_radio_value( radioName ) {
	var radios = document.getElementsByName( radioName );
	for( var i = 0; i < radios.length; i++ ) {
		if( radios[i].checked || radios.length === 1 ) {
			return radios[i].value;
		}
	}
}

function to_json(workbook) {
	var result = {};
	workbook.SheetNames.forEach(function(sheetName) {
		var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
		if(roa.length > 0){
			result[sheetName] = roa;
		}
	});
	return result;
}

function to_csv(workbook) {
	var result = [];
	workbook.SheetNames.forEach(function(sheetName) {
		var csv = X.utils.sheet_to_csv(workbook.Sheets[sheetName]);
		if(csv.length > 0){
			result.push("SHEET: " + sheetName);
			result.push("");
			result.push(csv);
		}
	});
	return result.join("\n");
}

function to_formulae(workbook) {
	var result = [];
	workbook.SheetNames.forEach(function(sheetName) {
		var formulae = X.utils.get_formulae(workbook.Sheets[sheetName]);
		if(formulae.length > 0){
			result.push("SHEET: " + sheetName);
			result.push("");
			result.push(formulae.join("\n"));
		}
	});
	return result.join("\n");
}



function process_wb(wb) {
	var output = "";
	switch(get_radio_value("format")) {
		case "json":
			output = JSON.stringify(to_json(wb), 2, 2);
			var obj = JSON.parse(output);

			setFieldValues (obj);
			break;
		case "form":
			output = to_formulae(wb);
			break;
		default:
		output = to_csv(wb);
	}
	if(out.innerText === undefined) out.textContent = output;
	else out.innerText = output;
	if(typeof console !== 'undefined') console.log("output", new Date());
}



//DRAG DROP FUNCTIONS
var drop = document.getElementById('drop');
function handleDrop(e) {
	e.stopPropagation();
	e.preventDefault();
	rABS = document.getElementsByName("userabs")[0].checked;
	use_worker = document.getElementsByName("useworker")[0].checked;
	var files = e.dataTransfer.files;
	var f = files[0];
	{
		var reader = new FileReader();
		var name = f.name;
		reader.onload = function(e) {
			if(typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
			var data = e.target.result;
			if(use_worker) {
				xw(data, process_wb);
			} else {
				var wb;
				if(rABS) {
					wb = X.read(data, {type: 'binary'});
				} else {
				var arr = fixdata(data);
					wb = X.read(btoa(arr), {type: 'base64'});
				}
				process_wb(wb);
			}
		};
		if(rABS) reader.readAsBinaryString(f);
		else reader.readAsArrayBuffer(f);
	}
}

function handleDragover(e) {
	e.stopPropagation();
	e.preventDefault();
	e.dataTransfer.dropEffect = 'copy';
}


//DROP LISTENERS
if(drop.addEventListener) {
	drop.addEventListener('dragenter', handleDragover, false);
	drop.addEventListener('dragover', handleDragover, false);
	drop.addEventListener('drop', handleDrop, false);
}


var xlf = document.getElementById('xlf');
function handleFile(e) {
	rABS = document.getElementsByName("userabs")[0].checked;
	use_worker = document.getElementsByName("useworker")[0].checked;
	var files = e.target.files;
	var f = files[0];
	{
		var reader = new FileReader();
		var name = f.name;
		reader.onload = function(e) {
			if(typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
			var data = e.target.result;
			if(use_worker) {
				xw(data, process_wb);
			} else {
				var wb;
				if(rABS) {
					wb = X.read(data, {type: 'binary'});
				} else {
				var arr = fixdata(data);
					wb = X.read(btoa(arr), {type: 'base64'});
				}
				process_wb(wb);
			}
		};
		if(rABS) reader.readAsBinaryString(f);
		else reader.readAsArrayBuffer(f);
	}
}

if(xlf.addEventListener) xlf.addEventListener('change', handleFile, false);





function setFieldValues ( obj ) {

			var report = obj[Object.keys(obj)[0]];

			if( report != null ) {			
				var r_date;
				var r_forfait;
				var r_lang; 
				var r_pbs;
				var r_notes; 
				var r_length;
				var contract_length;
				var r_monthlyBudget; 
				var territory;
				var f_territory = -1;
				var r_territory;
				var variable_amount = "";
				var r_startDate;

				var d;
				var startDate;
				var endDate;		

				var spendType = "Fixed";
				var f_spendType = 11304;

				var managementfee;

				var f_lang;

				if ( report[0]["Date"] != null ) {
					r_date = report[0]["Date"];				
				}

				if ( report[0]["Nom du format - Français"] != null ) { 
					r_forfait = report[0]["Nom du format - Français"].toLowerCase();
				}
			
				if ( report[0]["Languages"] != null ) {
					r_lang	= report[0]["Languages"].toLowerCase();		
				}

				if ( report[0]["Id PBS-WSTD"] != null ) {
					r_pbs	= report[0]["Id PBS-WSTD"];
				}

				if ( report[0]["Notes"] != null ) {
					r_notes = report[0]["Notes"];	
				}
				
				if ( report[0]["Durée de l'entente"] != null ) {
					r_length = report[0]["Durée de l'entente"].toLowerCase();
					contract_length = parseInt(r_length.replace(" mois", ""));
				} else {
					r_length = "";
					contract_length = "";
				}
				
				if ( report[0]["Montant du produit"] != null )					 {
					r_monthlyBudget = parseInt(report[0]["Montant du produit"]);	
				} 
				

				if ( report[0]["Région/Province de l'adresse de facturation"] != null ) {
					r_territory = report[0]["Région/Province de l'adresse de facturation"].toLowerCase();
				}

				if ( report[0]["Région/Province de facturation"] != null ) { 
					r_territory = report[0]["Région/Province de facturation"].toLowerCase();
				}

				if ( r_territory != "quebec" ) {
						territory = "ACS";
						f_territory = 11307;
					} else {
						territory = "QC";
						f_territory = 11306;
					} 

				if ( report[0]["Date de début de l'entente"] != null ) {
					r_startDate = report[0]["Date de début de l'entente"];

				} else {
					r_startDate = r_date;
				}

				if ( r_date != null && r_startDate != null  ) {
					d = new Date(r_startDate);
					startDate = new Date(new Date(d).setMonth(d.getMonth()));
					endDate = new Date(new Date(d).setMonth(d.getMonth()+contract_length));

					endDate = new Date(endDate.setDate(endDate.getDate() - 1));
					startDate = (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' +  startDate.getFullYear();
					endDate = (endDate.getMonth() + 1) + '/' + endDate.getDate() + '/' +  endDate.getFullYear();					
				}
				
				if ( r_forfait != null ) { 
					if (r_forfait.indexOf("sur mesure") > -1 || r_forfait.indexOf("Renouvellement Forfait Platine +") > -1 ) {
						
						//forfait sur mesure
						spendType = "Variable";
						f_spendType = 11305;

						var sommeBudget = 0;

						for (i = 0; i < contract_length; i++) { 

							if ( report[i] != null && report[i]["Montant du produit"]  != null && report[i]["Montant du produit"] > 0.01 ) { 
		    				variable_amount += report[i]["Montant du produit"] + ";";
		    				sommeBudget += parseInt(report[i]["Montant du produit"]);
		    			} else {
		    				variable_amount = "";
		    				break;
		    			}
						}

						variable_amount = variable_amount.slice(0, -1);
						r_monthlyBudget = sommeBudget / contract_length;

					}

				} 

				if ( r_monthlyBudget != null ) { 

					if(r_monthlyBudget >= 500 && r_monthlyBudget < 750 ){
						managementfee = 35;
					} else if ( r_monthlyBudget >= 750 ){
						managementfee = 33;
					} else {
						managementfee = 37;
					}

				}

				if ( r_lang == "français" ) {
					f_lang = 11299;
				} else if ( r_lang == "english" ) {
					f_lang = 11300;
				} else if (r_lang == "bilingual") {
					f_lang = 11301;
				} else {
					f_lang = -1;
				}

				var r_salesRep = (typeof  report[0]["Sales Rep: Nom complet"] === 'undefined') ? "" :  report[0]["Sales Rep: Nom complet"];  
				var r_dv = (typeof  report[0]["Directeur des ventes"] === 'undefined') ? "" :  report[0]["Directeur des ventes"]; 
				var r_company = (typeof  report[0]["Nom commercial"] === 'undefined') ? (typeof  report[0]["Client"] === 'undefined') ? "" :  report[0]["Client"] :  report[0]["Nom commercial"]; 
				var r_company_street = (typeof  report[0]["Rue de l'adresse de facturation"] === 'undefined') ? "" :  report[0]["Rue de l'adresse de facturation"];
				var r_company_city = (typeof  report[0]["Ville de l'adresse de facturation"] === 'undefined') ? "" :  report[0]["Ville de l'adresse de facturation"];
				var r_company_region = (typeof report[0]["Région/Province de facturation"] === 'undefined') ? (typeof report[0]["Région/Province de l'adresse de facturation"] === 'undefined') ? "" : report[0]["Région/Province de l'adresse de facturation"] : report[0]["Région/Province de facturation"]; 
				var r_company_postal = (typeof  report[0]["Code postal de l'adresse de facturation"] === 'undefined') ? "" :  report[0]["Code postal de l'adresse de facturation"];
				var r_company_phone = (typeof  report[0]["Téléphone"] === 'undefined') ? "" :  report[0]["Téléphone"];
				var r_contact_name = (typeof report[0]["Nom du contact"] === 'undefined') ? "" : report[0]["Nom du contact"]; 	  
				var r_contact_phone = (typeof report[0]["Contact Phone"] === 'undefined') ? "" : report[0]["Contact Phone"]; 	 
				var r_contact_email = (typeof report[0]["Courriel 1"] === 'undefined') ? "" : report[0]["Courriel 1"]; 	  
				var r_url = (typeof report[0]["URL 1"] === 'undefined') ? "" : report[0]["URL 1"]; 	  
				var r_services_to_promote = (typeof report[0]["Services à promouvoir"] === 'undefined') ? "" : report[0]["Services à promouvoir"];
				var r_show_address = (typeof report[0]["Show Address"] === 'undefined') ? "" : report[0]["Show Address"];
				var r_campaign_lang = (typeof report[0]["Languages"] === 'undefined') ? "" : report[0]["Languages"]; 	
				var r_CampaignStartDate = (typeof startDate === 'undefined') ? "" : startDate; 	
				var r_CampaignEndDate = (typeof endDate === 'undefined') ? "" : endDate; 	
				var r_notes = (typeof report[0]["Notes"] === 'undefined') ? "" : report[0]["Notes"]; 	
				
				var r_description = 

														"Nom commercial : " + r_company + "\n" + 

														"Sales Rep: Nom complet : " + r_salesRep + "\n" +
														"Directeur des ventes : " + r_dv  + "\n\n" + 

														"Rue de l'adresse de facturation :" + r_company_street + "\n" + 
														"Ville de l'adresse de facturation : " +  r_company_city + "\n" + 
														"Région/Province de facturation : " + r_company_region + "\n" + 
														"Code postal de l'adresse de facturation : " + r_company_postal + "\n\n" + 

														"Date de début de l'entente : " + r_CampaignStartDate + "\n" + 
														"Date de fin de l'entente : " + r_CampaignEndDate + "\n" + 
														"Durée de l'entente : " + r_length +  " / " + r_monthlyBudget + "$ par mois / " + managementfee + "% \n\n" + 

														"Nom du contact : " + r_contact_name + "\n" +  												
														"Courriel 1 : " + r_contact_email +  "\n" + 
														"Téléphone : " + r_company_phone + "\n" +  
														"URL 1 : " +  r_url + "\n" +  
														"Services à promouvoir : " + r_services_to_promote + "\n" +  
														"Contact Phone : " + r_contact_phone + "\n" +  
														"Show Address : " + r_show_address + "\n" +  
														"Language : " + r_campaign_lang + "\n\n" +
														"Notes : " + r_notes;		

				document.getElementById("sem-output").innerHTML = (typeof r_description === 'undefined') ? "" : r_description; 
				document.getElementById("i_company").value = (typeof r_company === 'undefined') ? "" : r_company;
				document.getElementById("i_description").value = (typeof r_description === 'undefined') ? "" : r_description;
				document.getElementById("i_pbs").value = (typeof r_pbs === 'undefined') ? "" : r_pbs;
				document.getElementById("i_startDate").value = (typeof startDate === 'undefined') ? "" : startDate;
				document.getElementById("i_endDate").value = (typeof endDate === 'undefined') ? "" : endDate;
				document.getElementById("i_spendType").value = (typeof spendType === 'undefined') ? "" : spendType;
				document.getElementById("i_monthlyBudget").value = (typeof r_monthlyBudget === 'undefined') ? "" : r_monthlyBudget;
				document.getElementById("i_varBudget").value = (typeof variable_amount === 'undefined') ? "" : variable_amount;
				document.getElementById("i_managementfee").value = (typeof managementfee === 'undefined') ? "" : managementfee;
				document.getElementById("i_territory").value = (typeof territory === 'undefined') ? "" : territory;
				document.getElementById("i_campaign_lang").value = (typeof r_campaign_lang === 'undefined') ? "" : r_campaign_lang;

			} //end if report if null
}

function setURL() {
	//Staging

	//spendtype
	var value_spendType = document.getElementById("i_spendType").value.toLowerCase();
	var f_spendType = -1;

	if ( value_spendType == "fixed") {
		//f_spendType = 11304; //staging
		f_spendType = 11402; //prod
	} else if ( value_spendType == "variable") {
		//f_spendType = 11305; //staging
		f_spendType = 11403; //prod
	} 

	//territory
	var value_territory = document.getElementById("i_territory").value.toLowerCase();
	var f_territory = -1;

	if ( value_territory == "qc") {
		//f_territory = 11306; //staging
		f_territory = 11404; //prod
	} else if ( value_territory == "acs") {
		//f_territory = 11307; //staging
		f_territory = 11405; //prod
	} 

//
	//LANG
	var value_lang = document.getElementById("i_campaign_lang").value.toLowerCase();
	var f_lang = -1;
	if ( value_lang == "français") {
		//f_lang = 11299; //staging
		f_lang = 11397; //prod
	} else if ( value_lang == "english") {
		//f_lang = 11300; //staging
		f_lang = 11398; //prod
	} else if ( value_lang == "bilingual") {
		//f_lang = 11301; //staging
		f_lang = 11399; //prod
	} 	

	var f_var_budget = document.getElementById("i_varBudget").value;




	//staging 
	/*
	params = {
	        pid: 11070,
	        issuetype: 39,
	        summary: document.getElementById("i_company").value,
	        description: document.getElementById("i_description").value,        
	        customfield_10233:document.getElementById("i_startDate").value,
	        customfield_10221:document.getElementById("i_endDate").value,
	        customfield_10970:f_lang,
	        customfield_10981:document.getElementById("i_managementfee").value,
	        customfield_10972:document.getElementById("i_monthlyBudget").value,
	        customfield_10973:document.getElementById("i_pbs").value,
	        customfield_10975:f_spendType,
	        customfield_10976:f_territory
	    }
*/
	    
	//Prod
	params = {
	        pid: 11076,
	        issuetype: 39,
	        summary: document.getElementById("i_company").value,
	        description: document.getElementById("i_description").value,
	        components:11292,
	        customfield_10233:document.getElementById("i_startDate").value,
	        customfield_10867:document.getElementById("i_endDate").value,
	        customfield_10871:f_lang,
	        customfield_10872:document.getElementById("i_managementfee").value,
	        customfield_10873:document.getElementById("i_monthlyBudget").value,
	        customfield_10874:document.getElementById("i_pbs").value,
	        customfield_10876:f_spendType,
	        customfield_10877:f_territory,
	        customfield_10878:f_var_budget
					
	    }
	
	var str = jQuery.param( params );
	document.getElementById("jira-url").value = "http://jira.transcontinental.ca/secure/CreateIssueDetails!init.jspa?" + str 
	//document.getElementById("jira-url").value = "http://staging.jira.transcontinental.ca/secure/CreateIssueDetails!init.jspa?" + str 

}

function gotoURL() {

	setURL();
	window.location.href = document.getElementById("jira-url").value;
	return false;
}


</script>

<script>
<!--

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65922209-5', 'auto');
  ga('set', 'checkProtocolTask', function(){});
  ga('send', {'hitType': 'pageview', 'page': '/sem-newticket-fr'}); 

-->
</script>


</body>
</html>
<!--[if gte mso 9]><![endif]-->

<!--[if gte mso 9]><![endif]-->
</head><![endif]-->